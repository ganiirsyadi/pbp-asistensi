from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("autentikasi:signin")
        return render(request, "autentikasi/signup.html")
    else:
        return render(request, "autentikasi/signup.html")

def signin(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("homepage:index")
        return render(request, "autentikasi/signin.html")
    else:
        return render(request, "autentikasi/signin.html")

def signout(request):
    logout(request)
    return redirect("homepage:index")