from django.urls import path
from .views import signup, signin, signout

app_name = 'autentikasi'

urlpatterns = [
    path('signin/', signin, name="signin"),
    path('signup/', signup, name="signup"),
    path('signout/', signout, name="signout"),
]