from django.urls import path
from .views import index, add_note

app_name = 'homepage'

urlpatterns = [
    path('', index, name="index"),
    path('add_note/', add_note, name="add_note")
]