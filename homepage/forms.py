from django.forms import ModelForm

from homepage.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['title', 'datetime', 'content']
