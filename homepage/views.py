from django.shortcuts import render, redirect

from homepage.forms import NoteForm
from .models import Note
from django.contrib.auth.decorators import login_required

# Create your views here.


def index(request):
    notes = Note.objects.all()
    data = {"daftar_note": notes}
    return render(request, template_name="homepage/index_with_bootstrap.html", context=data)

# @login_required(login_url="autentikasi:signin")
# def add_note(request):
#     if request.method == "POST":
#         title = request.POST["title"]
#         datetime = request.POST["datetime"]
#         content = request.POST["content"]
#         Note.objects.create(
#             title=title,
#             datetime=datetime,
#             content=content
#         )
#     return redirect("homepage:index")

@login_required(login_url="autentikasi:signin")
def add_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
    return redirect("homepage:index")