from django.forms import ModelForm, widgets
from django.forms.widgets import Input
from .models import ToDo

class TodoForm(ModelForm):
    class Meta:
        model = ToDo
        fields = "__all__"
        widgets = {
            "deadline": Input(attrs={"type": "datetime-local", "class": "form-control"}),
            "text": Input(attrs={"class": "form-control"}),
        }
