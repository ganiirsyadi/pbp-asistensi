from django.urls import path
from .views import *

app_name = "todo"

urlpatterns = [
    path("", index, name="index"),
    path("api/", get_todos, name="api_todo"),
    path("add/", add_todo, name="add_todo"),
    path("delete/<str:todo_id>", delete_todo, name="delete_todo"),
]
