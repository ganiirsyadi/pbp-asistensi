from typing import final
from django.shortcuts import redirect, render

from todo.forms import TodoForm
from .models import ToDo
from django.core import serializers
from django.http.response import HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url="autentikasi:signin")
def index(request):
    return render(request, "todo/index.html")

@login_required(login_url="autentikasi:signin")
def get_todos(request):
    todos = ToDo.objects.all()
    todos_json = serializers.serialize("json", todos)
    return HttpResponse(todos_json, content_type="application/json")

@login_required(login_url="autentikasi:signin")
def add_todo(request):
    form = TodoForm()
    if request.method == "POST":
        data = request.POST.dict()
        data["user"] = request.user
        form = TodoForm(data)
        if form.is_valid():
            form.save()
            return redirect("todo:index")
    context = {"form": form}
    return render(request, "todo/add_todo.html", context)


@login_required(login_url="autentikasi:signin")
def delete_todo(request, todo_id):
    try:
        ToDo.objects.get(id=todo_id).delete()
    except Exception as e:
        print(e)
    finally:
        return redirect("todo:index")