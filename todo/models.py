from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class ToDo(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deadline = models.DateTimeField()
    text = models.CharField(max_length=150)

    def __str__(self):
        return f"{self.user.username} | {self.text}"
